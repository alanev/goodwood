import Tabs from '../modules/intro/intro'

new Tabs('.intro__dot', '.intro__slide')

Array.from(
	document.querySelectorAll('.benefit')
).forEach(benefit => {
	new Tabs('.benefit__tab', '.benefit__content', benefit)
})

require('../modules/g-section/g-section')
