export default class Intro {
	constructor(buttonsSelecor, contentsSelector, parent) {
		this.buttons = Array.from((parent || document).querySelectorAll(buttonsSelecor))
		this.contents = Array.from((parent || document).querySelectorAll(contentsSelector))
		this.active = 0
		this.className = '_active'

		this._buttons()
	}
	_select(index) {
		const {
			contents,
			buttons,
			active,
			className,
		} = this
		buttons[active].classList.remove(className)
		buttons[index].classList.add(className)

		contents[active].classList.remove(className)
		contents[index].classList.add(className)

		this.active = index
	}
	_buttons() {
		this.buttons.forEach((button, index) => {
			button.addEventListener('click', () => this._select(index))
		})
	}
}
