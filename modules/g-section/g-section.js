import map from '../map/map'

function isElementInViewport (el) {

	const { top } = el.getBoundingClientRect()
	const { innerHeight } = window

	return (
		top >= 0 &&
		top <= innerHeight
	)
}

let sections = Array.from(document.querySelectorAll('.g-section'))

sections.forEach(section => section.classList.add('_NO_VISIBLE'))

function handler() {
	sections.forEach((section, index) => {
		if (isElementInViewport(section)){
			section.classList.remove('_NO_VISIBLE')
			section.classList.add('_VISIBLE')
			if (section.classList.contains('g-section--map')){
				map()
			}
			sections.splice(index, 1)
		}
	})
}

window.addEventListener('scroll', handler)
window.addEventListener('resize', handler)
window.addEventListener('load', handler)
