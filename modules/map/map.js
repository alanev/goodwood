import CountUp from 'countup.js'

const numbers =
	Array.from(
		document.querySelectorAll('.map__numb')
	)
const map = document.querySelector('.map__russia')

export default function () {
	numbers.forEach(element => {
		(new CountUp(
			element,
			0,
			element.innerText,
		)).start()
	})
}
